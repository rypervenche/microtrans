//TODO: Put the xz files somewhere or in the binary
mod clap;
mod locales;
mod xml;

use crate::clap::*;
use crate::locales::*;
use crate::xml::*;
use ::clap::Parser;
use anyhow::Result;
use once_cell::sync::Lazy;
use serde_xml_rs::from_str;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use xz2::read::XzDecoder;

/// Parse CLI options
static OPTS: Lazy<Opts> = Lazy::new(Opts::parse);

fn read_input() -> Result<String> {
    let mut input = String::new();
    let stdin = io::stdin(); // We get `Stdin` here.
    stdin.read_line(&mut input)?;
    Ok(input)
}

fn clear() -> Result<()> {
    print!("\x1B[2J\x1B[1;1H");
    io::stdout().flush()?;
    Ok(())
}

fn main() -> Result<()> {
    let _ = *OPTS;
    let locale = match &OPTS.locale {
        Some(l) => l.to_owned(),
        None => Locale::default(),
    }
    .to_string();
    let xz_lang_file = format!("./resources/{locale}.tbx.xz");
    let file = File::open(xz_lang_file).expect("file not found!");
    let buf_reader = BufReader::new(file);
    let mut decompressor = XzDecoder::new(buf_reader);

    let mut contents = String::new();
    decompressor.read_to_string(&mut contents)?;

    let translations: Translations = from_str(&contents)?;

    let mut entries: Entries = Entries::default();

    for term_entry in translations.text.body.entries {
        let en_term: &str = term_entry.lang_sets[0].ntig[0].term_grp.term[0].as_ref();
        let en_desc = term_entry.lang_sets[0].descrip_grp.as_ref().unwrap();
        let en_desc: &str = en_desc.descrip.as_ref();
        let tl_term: &str = term_entry.lang_sets[1].ntig[0].term_grp.term[0].as_ref();

        let vec = Entry {
            en_term: en_term.to_string(),
            en_desc: en_desc.to_string(),
            tl_term: tl_term.to_string(),
        };

        entries.add(vec);
    }

    clear()?;

    loop {
        println!("Enter search word:");
        let search_word = read_input()?;
        let search_word = search_word.trim();
        println!();

        if search_word == "q" {
            break;
        }

        let results = entries.search_strings(search_word);
        clear()?;
        results.print();
    }
    Ok(())
}
