use strum::{Display, EnumString};

#[derive(EnumString, Display, Default, Clone, Debug)]
#[allow(non_camel_case_types)]
pub(crate) enum Locale {
    #[strum(serialize = "af-ZA")]
    af_ZA,
    #[strum(serialize = "sq-AL")]
    sq_AL,
    #[strum(serialize = "am-ET")]
    am_ET,
    #[strum(serialize = "ar-SA")]
    ar_SA,
    #[strum(serialize = "hy-AM")]
    hy_AM,
    #[strum(serialize = "as-IN")]
    as_IN,
    #[strum(serialize = "az-Latn-AZ")]
    az_Latn_AZ,
    #[strum(serialize = "bn-BD")]
    bn_BD,
    #[strum(serialize = "bn-IN")]
    bn_IN,
    #[strum(serialize = "eu-ES")]
    eu_ES,
    #[strum(serialize = "be-BY")]
    be_BY,
    #[strum(serialize = "bs-Cyrl-BA")]
    bs_Cyrl_BA,
    #[strum(serialize = "bs-Latn-BA")]
    bs_Latn_BA,
    #[strum(serialize = "bg-BG")]
    bg_BG,
    #[strum(serialize = "ca-ES")]
    ca_ES,
    #[strum(serialize = "ku-Arab-IQ")]
    ku_Arab_IQ,
    #[strum(serialize = "chr-Cher-US")]
    chr_Cher_US,
    #[strum(serialize = "zh-CN")]
    zh_CN,
    #[strum(serialize = "zh-HK")]
    zh_HK,
    #[default]
    #[strum(serialize = "zh-TW")]
    zh_TW,
    #[strum(serialize = "hr-HR")]
    hr_HR,
    #[strum(serialize = "cs-CZ")]
    cs_CZ,
    #[strum(serialize = "da-DK")]
    da_DK,
    #[strum(serialize = "prs-AF")]
    prs_AF,
    #[strum(serialize = "nl-NL")]
    nl_NL,
    #[strum(serialize = "en-GB")]
    en_GB,
    #[strum(serialize = "et-EE")]
    et_EE,
    #[strum(serialize = "fil-PH")]
    fil_PH,
    #[strum(serialize = "fi-FI")]
    fi_FI,
    #[strum(serialize = "fr-FR")]
    fr_FR,
    #[strum(serialize = "fr-CA")]
    fr_CA,
    #[strum(serialize = "gl-ES")]
    gl_ES,
    #[strum(serialize = "ka-GE")]
    ka_GE,
    #[strum(serialize = "de-DE")]
    de_DE,
    #[strum(serialize = "el-GR")]
    el_GR,
    #[strum(serialize = "gu-IN")]
    gu_IN,
    #[strum(serialize = "ha-Latn-NG")]
    ha_Latn_NG,
    #[strum(serialize = "he-IL")]
    he_IL,
    #[strum(serialize = "hi-IN")]
    hi_IN,
    #[strum(serialize = "hu-HU")]
    hu_HU,
    #[strum(serialize = "is-IS")]
    is_IS,
    #[strum(serialize = "ig-NG")]
    ig_NG,
    #[strum(serialize = "id-ID")]
    id_ID,
    #[strum(serialize = "iu-Latn-CA")]
    iu_Latn_CA,
    #[strum(serialize = "ga-IE")]
    ga_IE,
    #[strum(serialize = "xh-ZA")]
    xh_ZA,
    #[strum(serialize = "zu-ZA")]
    zu_ZA,
    #[strum(serialize = "it-IT")]
    it_IT,
    #[strum(serialize = "ja-JP")]
    ja_JP,
    #[strum(serialize = "quc-Latn-GT")]
    quc_Latn_GT,
    #[strum(serialize = "kn-IN")]
    kn_IN,
    #[strum(serialize = "kk-KZ")]
    kk_KZ,
    #[strum(serialize = "km-KH")]
    km_KH,
    #[strum(serialize = "rw-RW")]
    rw_RW,
    #[strum(serialize = "sw-KE")]
    sw_KE,
    #[strum(serialize = "kok-IN")]
    kok_IN,
    #[strum(serialize = "ko-KR")]
    ko_KR,
    #[strum(serialize = "ky-KG")]
    ky_KG,
    #[strum(serialize = "lo-LA")]
    lo_LA,
    #[strum(serialize = "lv-LV")]
    lv_LV,
    #[strum(serialize = "lt-LT")]
    lt_LT,
    #[strum(serialize = "lb-LU")]
    lb_LU,
    #[strum(serialize = "mk-MK")]
    mk_MK,
    #[strum(serialize = "ms-BN")]
    ms_BN,
    #[strum(serialize = "ms-MY")]
    ms_MY,
    #[strum(serialize = "ml-IN")]
    ml_IN,
    #[strum(serialize = "mt-MT")]
    mt_MT,
    #[strum(serialize = "mi-NZ")]
    mi_NZ,
    #[strum(serialize = "mr-IN")]
    mr_IN,
    #[strum(serialize = "mn-MN")]
    mn_MN,
    #[strum(serialize = "ne-NP")]
    ne_NP,
    #[strum(serialize = "nb-NO")]
    nb_NO,
    #[strum(serialize = "nn-NO")]
    nn_NO,
    #[strum(serialize = "or-IN")]
    or_IN,
    #[strum(serialize = "ps-AF")]
    ps_AF,
    #[strum(serialize = "fa-IR")]
    fa_IR,
    #[strum(serialize = "pl-PL")]
    pl_PL,
    #[strum(serialize = "pt-BR")]
    pt_BR,
    #[strum(serialize = "pt-PT")]
    pt_PT,
    #[strum(serialize = "pa-IN")]
    pa_IN,
    #[strum(serialize = "pa-Arab-PK")]
    pa_Arab_PK,
    #[strum(serialize = "quz-PE")]
    quz_PE,
    #[strum(serialize = "ro-RO")]
    ro_RO,
    #[strum(serialize = "ru-RU")]
    ru_RU,
    #[strum(serialize = "gd-GB")]
    gd_GB,
    #[strum(serialize = "sr-Cyrl-BA")]
    sr_Cyrl_BA,
    #[strum(serialize = "sr-Cyrl-RS")]
    sr_Cyrl_RS,
    #[strum(serialize = "sr-Latn-RS")]
    sr_Latn_RS,
    #[strum(serialize = "nso-ZA")]
    nso_ZA,
    #[strum(serialize = "tn-ZA")]
    tn_ZA,
    #[strum(serialize = "sd-Arab-PK")]
    sd_Arab_PK,
    #[strum(serialize = "si-LK")]
    si_LK,
    #[strum(serialize = "sk-SK")]
    sk_SK,
    #[strum(serialize = "sl-SI")]
    sl_SI,
    #[strum(serialize = "es-ES")]
    es_ES,
    #[strum(serialize = "es-MX")]
    es_MX,
    #[strum(serialize = "sv-SE")]
    sv_SE,
    #[strum(serialize = "tg-Cyrl-TJ")]
    tg_Cyrl_TJ,
    #[strum(serialize = "ta-IN")]
    ta_IN,
    #[strum(serialize = "tt-RU")]
    tt_RU,
    #[strum(serialize = "te-IN")]
    te_IN,
    #[strum(serialize = "th-TH")]
    th_TH,
    #[strum(serialize = "ti-ET")]
    ti_ET,
    #[strum(serialize = "tr-TR")]
    tr_TR,
    #[strum(serialize = "tk-TM")]
    tk_TM,
    #[strum(serialize = "uk-UA")]
    uk_UA,
    #[strum(serialize = "ur-PK")]
    ur_PK,
    #[strum(serialize = "ug-CN")]
    ug_CN,
    #[strum(serialize = "uz-Latn-UZ")]
    uz_Latn_UZ,
    #[strum(serialize = "ca-ES-valencia")]
    ca_ES_valencia,
    #[strum(serialize = "vi-VN")]
    vi_VN,
    #[strum(serialize = "guc-VE")]
    guc_VE,
    #[strum(serialize = "cy-GB")]
    cy_GB,
    #[strum(serialize = "wo-SN")]
    wo_SN,
    #[strum(serialize = "yo-NG")]
    yo_NG,
}
