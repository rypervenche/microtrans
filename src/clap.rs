use crate::locales::Locale;
use clap::Parser;

/// Command line options
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub(crate) struct Opts {
    /// Optional locale
    #[arg(short, long)]
    pub locale: Option<Locale>,

    /// Search through description as well as name
    #[arg(short = 'b', long)]
    pub search_description: bool,
}
