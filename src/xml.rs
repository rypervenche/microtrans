use crate::OPTS;
use ansi_term::Colour::White;
use ansi_term::Colour::Yellow;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub(crate) struct Translations {
    pub text: Text,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct Text {
    pub body: TermEntries,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct TermEntries {
    #[serde(rename = "termEntry")]
    pub entries: Vec<TermEntry>,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct TermEntry {
    #[serde(rename = "langSet")]
    pub lang_sets: Vec<LangSet>,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct LangSet {
    pub descrip_grp: Option<DescripGrp>,
    pub ntig: Vec<Ntig>,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct Ntig {
    pub term_grp: TermGrp,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct DescripGrp {
    pub descrip: String,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct TermGrp {
    pub term: Vec<String>,
    pub term_note: Vec<String>,
}

#[derive(Clone, Default)]
pub(crate) struct Entries {
    pub entries: Vec<Entry>,
}

impl Entries {
    pub fn add(&mut self, entry: Entry) {
        self.entries.push(entry);
    }

    pub fn search_strings(&self, query: &str) -> Entries {
        // let mut vec = Vec::new();
        let entries = self
            .entries
            .clone()
            .into_iter()
            .filter(|entry| {
                if OPTS.search_description {
                    entry
                        .en_term
                        .to_lowercase()
                        .contains(query.to_lowercase().as_str())
                        || entry
                            .en_desc
                            .to_lowercase()
                            .contains(query.to_lowercase().as_str())
                } else {
                    entry
                        .en_term
                        .to_lowercase()
                        .contains(query.to_lowercase().as_str())
                }
            })
            .collect::<Vec<Entry>>();
        Entries { entries }
    }

    pub fn print(&self) {
        for entry in &self.entries {
            println!(
                // println!("{}", Green.bold().paint("IP address unchanged."));
                "EN: {}\nTL: {}\nDESC: {}\n",
                White.bold().paint(&entry.en_term),
                Yellow.bold().paint(&entry.tl_term),
                entry.en_desc
            );
        }
    }
}

#[derive(Clone)]
pub(crate) struct Entry {
    pub en_term: String,
    pub en_desc: String,
    pub tl_term: String,
}
